﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonStuff : MonoBehaviour {
	// ****************************************
	// Fields
	// ****************************************
	public static CommonStuff instance;

	public List<GameObject> firstRoomGoggles = new List<GameObject>();
	public List<GameObject> secondRoomGoggles = new List<GameObject>();
	public List<GameObject> thirdRoomGoggles = new List<GameObject>();
	public List<GameObject> forthRoomGoggles = new List<GameObject>();

	public GameObject firstDoor;
	public GameObject secondDoor;
	public GameObject thirdDoor;
	public GameObject forthDoor;

	public GameObject winScreen;

	private GameObject mainCamera;
	private SuperEffects fx;

	// ****************************************
	// Overriden framework's methods
	// ****************************************
	// Use this for initialization.
	void Start () {
		instance = this;
		mainCamera = GameObject.FindWithTag (PlayerController.PLAYER_CAMERA_TAG);
		fx = mainCamera.GetComponent<SuperEffects> ();
	}

	// ****************************************
	// Private methods
	// ****************************************
   private void TryToggleNextGoggle(List<GameObject> goggleList, bool isLastRoom)
   {
      foreach(GameObject goggleHolderItem in goggleList)
      {
         PickUpItem currentPickUpItem = goggleHolderItem.GetComponent<PickUpItem>();

         if (isLastRoom)
         {
            if (currentPickUpItem.isGood && currentPickUpItem.isPickedUp)
            {
               Victory();
               return;
            }
         }

         GameObject glasses = goggleHolderItem.transform.Find("Glasses").gameObject;

         if (!currentPickUpItem.isPickedUp && !currentPickUpItem.isEnabled)
         {
            currentPickUpItem.isEnabled = true;
            glasses.SetActive(true);
            return;
         }
      }
   }

   private void Victory () {
		winScreen.SetActive (true);
	}

	// ****************************************
	// Public methods
	// ****************************************
	public void OpenDoor(int roomId) {
		switch (roomId) {
			case 0:
				firstDoor.SetActive (false);
				break;
			case 1:
				secondDoor.SetActive (false);
				break;
			case 2:
				thirdDoor.SetActive (false);
				break;
			case 3:
				forthDoor.SetActive (false);
				break;
		}
	}

	public void ToggleNextGoggle(int roomId) {
		switch (roomId) {
			case 0:
				TryToggleNextGoggle (firstRoomGoggles, false);
				break;
			case 1:
				TryToggleNextGoggle (secondRoomGoggles, false);
				break;
			case 2:
				TryToggleNextGoggle (thirdRoomGoggles, false);
				break;
			case 3:
				TryToggleNextGoggle (forthRoomGoggles, true);
				break;
		}
	}

	public void ApplyVisualEffect (bool isGood) {
		if (isGood) {
			fx.GoodEffect ();
		} else {
			fx.BadEffect ();
		}
	}
}