﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Launch : MonoBehaviour {

	public GameObject[] toDestroy;
	public GameObject[] toEnable;
	public GameObject[] toDisable;
	public SwitchColor switchColor;

	// Use this for initialization
	void Start () {


	}

	public void LaunchGame()
	{
		//UnityEngine.SceneManagement.SceneManager.LoadScene (1);
		for (int i = 0; i < toDestroy.Length; i++) {
			Destroy (toDestroy[i]);
		}

		for (int i = 0; i < toEnable.Length; i++) {
			toEnable[i].SetActive (true);
		}

		for (int i = 0; i < toDisable.Length; i++) {
			toDisable[i].SetActive (false);
		}

		switchColor.enabled = false;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
		{
			LaunchGame();
		}
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}
