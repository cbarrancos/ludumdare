﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour {
	// ****************************************
	// Fields
	// ****************************************
	public bool isPickedUp = false;
	public bool isGood = false;
	public bool isEnabled = false;
	public int roomId;
	public AudioClip audioClip;
	private AudioSource sound;
   private GameObject glasses;

	// ****************************************
	// Overriden framework's methods
	// ****************************************
	// Use this for initialization.
	void Start () {
      glasses = transform.Find("Glasses").gameObject;
      glasses.SetActive (isEnabled);
		sound = GetComponentInChildren<AudioSource> ();
		sound.clip = audioClip;
	}

	// ****************************************
	// Public methods
	// ****************************************
	public void TakeMe() {
      if (isPickedUp || !isEnabled)
         return;

		isPickedUp = true;
		isEnabled = false;
      glasses.SetActive (false);

		if (isGood) {
			CommonStuff.instance.OpenDoor (roomId + 1);
		}

		CommonStuff.instance.ToggleNextGoggle (roomId);
		CommonStuff.instance.ApplyVisualEffect (isGood);
		sound.Play (1);
	}
}