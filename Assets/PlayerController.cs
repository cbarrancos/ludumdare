﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	// ****************************************
	// Constants
	// ****************************************
	public const string PLAYER_CAMERA_TAG = "MainCamera";
	private const float PICKUP_DISTANCE = 2f;

	// ****************************************
	// Fields
	// ****************************************
	private GameObject mainCamera;
	private List<PickUpItem> pickedUpItems = new List<PickUpItem>();

	// ****************************************
	// Overriden framework's methods
	// ****************************************
	// Use this for initialization.
	void Start () {
		mainCamera = GameObject.FindWithTag (PLAYER_CAMERA_TAG);
	}

	// Update is called once per frame.
	void Update () {
		ThievePickableItems ();
	}

	// ****************************************
	// Private methods
	// ****************************************
	private void ThievePickableItems() {
		if (Input.GetKeyDown (KeyCode.Q)) {
			int x = Screen.width / 2;
			int y = Screen.height / 2;
			Ray ray = mainCamera.GetComponent<Camera> ().ScreenPointToRay (new Vector3 (x, y));
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.transform.parent == null) {				
					return;
				}

				PickUpItem pickUpItem = hit.collider.transform.parent.GetComponent<PickUpItem> ();

				if (pickUpItem != null && pickUpItem.isEnabled && !pickUpItem.isPickedUp && hit.distance < PICKUP_DISTANCE) {
					pickedUpItems.Add (pickUpItem);
					pickUpItem.TakeMe();
				}
			}
		}
	}
}