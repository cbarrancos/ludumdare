﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(SuperEffects))]
public class SuperEffectsEditor : Editor
{
	public int currentColorShiftVariants = 0;
	public int currentChromaticAberrationVariants = 0;
	public int currentGrainVariants = 0;
	public int currentBloomVariants = 0;
	public int currentMotionBlurVariants = 0;
	public int currentDepthOfFieldVariants = 0;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		SuperEffects superEffects = (SuperEffects)target;
		if (superEffects == null) {
			return;
		}

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("Good"))
		{
			currentColorShiftVariants++;
			currentColorShiftVariants = currentColorShiftVariants % (superEffects.colorShiftVariants + 1);

			superEffects.ColorShift(currentColorShiftVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentColorShiftVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("Bad"))
		{
			currentColorShiftVariants++;
			currentColorShiftVariants = currentColorShiftVariants % (superEffects.colorShiftVariants + 1);

			superEffects.ColorShift(currentColorShiftVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentColorShiftVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		/*
		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("ColorShift"))
		{
			currentColorShiftVariants++;
			currentColorShiftVariants = currentColorShiftVariants % (superEffects.colorShiftVariants + 1);

			superEffects.ColorShift(currentColorShiftVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentColorShiftVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("ChromaticAberration"))
		{
			currentChromaticAberrationVariants++;
			currentChromaticAberrationVariants = currentChromaticAberrationVariants % (superEffects.chromaticAberrationVariants + 1);
			superEffects.ChromaticAberration(currentChromaticAberrationVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentChromaticAberrationVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("Grain"))
		{
			currentGrainVariants++;
			currentGrainVariants = currentGrainVariants % (superEffects.grainVariants + 1);
			superEffects.Grain(currentGrainVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentGrainVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("Bloom"))
		{
			currentBloomVariants++;
			currentBloomVariants = currentBloomVariants % (superEffects.bloomVariants + 1);

			superEffects.Bloom(currentBloomVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentBloomVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("MotionBlur"))
		{
			currentMotionBlurVariants++;
			currentMotionBlurVariants = currentMotionBlurVariants % (superEffects.motionBlurVariants + 1);
			superEffects.MotionBlur(currentMotionBlurVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentMotionBlurVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		if(GUILayout.Button("DepthOfField"))
		{
			currentDepthOfFieldVariants++;
			currentDepthOfFieldVariants = currentDepthOfFieldVariants % (superEffects.depthOfFieldVariants + 1);
			superEffects.DepthOfField(currentDepthOfFieldVariants);

			EditorSceneManager.NewPreviewScene();
		}
		GUILayout.Label (currentDepthOfFieldVariants.ToString (), GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();
		*/
	}
}