﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class SuperEffects : MonoBehaviour {

	public PostProcessingProfile profile;

	[HideInInspector]
	public int colorShiftVariants = 8;
	[HideInInspector]
	public int chromaticAberrationVariants = 3;
	[HideInInspector]
	public int grainVariants = 3;
	[HideInInspector]
	public int bloomVariants = 3;
	[HideInInspector]
	public int motionBlurVariants = 3;
	[HideInInspector]
	public int depthOfFieldVariants = 3;
	[HideInInspector]
	public int dogRainVariants = 1;

	public GameObject dogRain;

	public enum Effects
	{
		colorShift = 0,
		chromaticAberration = 1,
		grain = 2,
		bloom = 3,
		motionBlur = 4,
		depthOfField = 5,
		dogRain = 6,
	}

	public struct EffectsToAdd
	{
		public Effects effect;
		public int effectForce;

		public EffectsToAdd(Effects effect, int effectForce)
		{
			this.effect = effect;
			this.effectForce = effectForce;
		}
	}

	public List<List<EffectsToAdd>> effectsToAdd = new List<List<EffectsToAdd>>();

	public int currentGood = 0;

	void Awake ()
	{
		List<EffectsToAdd> list1 = new List<EffectsToAdd>();
		List<EffectsToAdd> list2 = new List<EffectsToAdd>();
		List<EffectsToAdd> list3 = new List<EffectsToAdd>();
		List<EffectsToAdd> list4 = new List<EffectsToAdd>();

		list1.Add (new EffectsToAdd (Effects.chromaticAberration, 1));

		list2.Add (new EffectsToAdd (Effects.chromaticAberration, 1));
		list2.Add (new EffectsToAdd (Effects.grain, 1));
		list2.Add (new EffectsToAdd (Effects.dogRain, 1));
		list2.Add (new EffectsToAdd (Effects.motionBlur, 1));
		list2.Add (new EffectsToAdd (Effects.depthOfField, 1));

		list3.Add (new EffectsToAdd (Effects.bloom, 2));
		list3.Add (new EffectsToAdd (Effects.grain, 2));
		list3.Add (new EffectsToAdd (Effects.bloom, 2));
		list3.Add (new EffectsToAdd (Effects.motionBlur, 2));
		list3.Add (new EffectsToAdd (Effects.depthOfField, 2));

		list4.Add (new EffectsToAdd (Effects.chromaticAberration, 3));
		list4.Add (new EffectsToAdd (Effects.grain, 3));
		list4.Add (new EffectsToAdd (Effects.bloom, 3));
		list4.Add (new EffectsToAdd (Effects.motionBlur, 3));
		list4.Add (new EffectsToAdd (Effects.depthOfField, 3));

		effectsToAdd.Add (list1);
		effectsToAdd.Add (list2);
		effectsToAdd.Add (list3);
		effectsToAdd.Add (list4);

		//basePosition = transform.localPosition;
		//wobblePosition = transform.localPosition;
		ColorShift(0);
		ChromaticAberration(0);
		Grain(0);
		Bloom(0);
		MotionBlur(0);
		DepthOfField (0);
		DogRain (0);
	}

	void OnDestroy()
	{
		ColorShift(0);
		ChromaticAberration(0);
		Grain(0);
		Bloom(0);
		MotionBlur(0);
		DepthOfField (0);
	}

	public void BadEffect()
	{
		if (effectsToAdd.Count == 0) {
			return;
		}

		int index = Random.Range (0, effectsToAdd [0].Count);
		Effects effect = effectsToAdd [0] [index].effect;
		if (effect == Effects.bloom) {
			Bloom (effectsToAdd [0] [index].effectForce);
		} else if (effect == Effects.chromaticAberration) {
			ChromaticAberration (effectsToAdd [0] [index].effectForce);
		} else if (effect == Effects.depthOfField) {
			DepthOfField (effectsToAdd [0] [index].effectForce);
		} else if (effect == Effects.grain) {
			Grain (effectsToAdd [0] [index].effectForce);
		} else if (effect == Effects.motionBlur) {
			MotionBlur (effectsToAdd [0] [index].effectForce);
		} else if (effect == Effects.dogRain) {
			DogRain (effectsToAdd [0] [index].effectForce);
		}
		effectsToAdd [0].RemoveAt (index);
		if (effectsToAdd [0].Count == 0)
		{
			effectsToAdd.RemoveAt (0);
		}
	}

	public void GoodEffect()
	{
		currentGood++;
		ColorShift (currentGood);
	}

	public void ColorShift(int i)
	{
		ColorGradingModel model = profile.colorGrading;
		ColorGradingModel.Settings settings = model.settings;
		settings.basic.hueShift = i * 45f;
		model .settings = settings;
		profile.colorGrading = model;
	}

	public void ChromaticAberration(int i)
	{
		ChromaticAberrationModel model = profile.chromaticAberration;
		ChromaticAberrationModel.Settings settings = model.settings;
		if (i == 0) {
			settings.intensity = 0f;
		} else if (i == 1) {
			settings.intensity = 10f;
		} else if (i == 2) {
			settings.intensity = 30f;
		} else {
			settings.intensity = 70f;
		}
		model.settings = settings;
		profile.chromaticAberration = model;
	}

	public void Grain(int i)
	{
		GrainModel model = profile.grain;
		GrainModel.Settings settings = model.settings;

		if (i == 0) {
			settings.intensity = 0f;
		} else if (i == 1) {
			settings.intensity = 0.5f;
			settings.size = 2.0f;
		} else if (i == 2) {
			settings.intensity = 2f;
			settings.size = 4.0f;
		} else {
			settings.intensity = 4f;
			settings.size = 8.0f;
		}
		model.settings = settings;
		profile.grain = model;
	}

	public void Bloom(int i)
	{
		BloomModel model = profile.bloom;
		BloomModel.Settings settings = model.settings;
		if (i == 0) {
			settings.bloom.intensity = 0f;
		} else if (i == 1) {
			settings.bloom.intensity = 10f;
			settings.bloom.threshold = 0.9f;
		} else if (i == 2) {
			settings.bloom.intensity = 20f;
			settings.bloom.threshold = 0.8f;
		} else {
			settings.bloom.intensity = 100f;
			settings.bloom.threshold = 0.7f;
		}
		model.settings = settings;
		profile.bloom = model;
	}

	public void MotionBlur(int i)
	{
		MotionBlurModel model = profile.motionBlur;
		MotionBlurModel.Settings settings = model.settings;
		if (i == 0) {
			settings.frameBlending = 0f;
		} else if (i == 1) {
			settings.frameBlending = 0.5f;
		} else if (i == 2) {
			settings.frameBlending = 1f;
		} else {
			settings.frameBlending = 2f;
		}
		model.settings = settings;
		profile.motionBlur = model;
	}

	public void DepthOfField(int i)
	{
		DepthOfFieldModel model = profile.depthOfField;
		DepthOfFieldModel.Settings settings = model.settings;
		if (i == 0) {
			settings.focusDistance = 10f;
			settings.focalLength = 50f;
			settings.aperture = 5.6f;
			settings.kernelSize = DepthOfFieldModel.KernelSize.Medium;
		} else if (i == 1) {
			settings.focusDistance = 1.05f;
			settings.focalLength = 14f;
			settings.aperture = 0.4f;
			settings.kernelSize = DepthOfFieldModel.KernelSize.Small;
		} else if (i == 2) {
			settings.focusDistance = 0.51f;
			settings.focalLength = 14f;
			settings.aperture = 0.4f;
			settings.kernelSize = DepthOfFieldModel.KernelSize.Large;
		}  else {
			settings.focusDistance = 0.1f;
			settings.focalLength = 14f;
			settings.aperture = 0.4f;
			settings.kernelSize = DepthOfFieldModel.KernelSize.VeryLarge;
		}
		model.settings = settings;
		profile.depthOfField = model;
	}


	public void DogRain(int i)
	{
		if (i == 0) 
		{
			dogRain.SetActive(false);
		} 
		else 
		{
			dogRain.SetActive(true);
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		/*
		if (Input.GetKeyDown(KeyCode.B))
		{
			BadEffect();
		}
		if (Input.GetKeyDown(KeyCode.G))
		{
			GoodEffect();
		}
		*/
	}

}
