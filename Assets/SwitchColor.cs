﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchColor : MonoBehaviour
{

    private UnityEngine.UI.Image background;
    private Color myColor;
    private float nuance = 0;

    // Use this for initialization
    void Start()
    {
        background = GetComponent<UnityEngine.UI.Image>();
        myColor = GetRandomColor();
    }

    // Update is called once per frame
    void Update()
    {
        nuance += Time.deltaTime;
        if(background.color == myColor)
        {
            myColor = GetRandomColor();
            nuance = 0;
        }
        background.color = Color.Lerp(background.color, myColor, nuance);
    }

    private Color GetRandomColor()
    {
        float color1 = Random.Range(0f, 1f);
        float color2 = Random.Range(0f, 1f);
        float color3 = Random.Range(0f, 1f);
        return new Color(color1, color2, color3);
    }
}